package com.suri.microservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by s.chintapalli on 7/8/2016.
 */
//@WebServlet("/SampleServlet")
//@WebServlet(value = "/SampleServlet", loadOnStartup = 1)
public class SampleServlet extends HttpServlet {

    private static Logger log = LoggerFactory.getLogger(SampleServlet.class);

    @Override
    public void init() throws ServletException {
        log.info("initializing init");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("calling doget");
        //resp.setHeader("Pragma", "public"); //$NON-NLS-1$ //$NON-NLS-2$
        //resp.setHeader("Cache-Control", properties.getProperty(Locator.CACHE_CONTROL)); //$NON-NLS-1$ //$NON-NLS-2$
        //resp.setHeader("X-XSS-protection", "1; mode=block");
        //resp.setHeader("X-Content-Type-Options", "nosniff");
        resp.getOutputStream().print("this is satya");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("calling dopost");
        super.doPost(req, resp);
    }
}
