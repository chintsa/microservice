package com.suri.microservice.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Created by s.chintapalli on 7/5/2016.
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Context
    HttpServletRequest request;

    @Context
    private ResourceInfo resourceInfo;

    private static Logger log = LoggerFactory.getLogger(AuthenticationFilter.class);

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Method method = resourceInfo.getResourceMethod();
        String resourceName = method.getDeclaringClass().getName() + "." + method.getName();
        log.info("filter::Running authentication filter for resource " + resourceName);

        //TODO: Pass the request object to getDecryptAndValidateUser and get User POJO.
        //try {User userInfo = getDecryptAndValidateUser(request);}catch(Exception e){}
        //TODO: if userInfo is null then return UNAUTHORIZED as follows
        //if(userInfo == null)
        //    requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
        //            .entity("You cannot access the resource " + method.getName()).build());
    }
}
