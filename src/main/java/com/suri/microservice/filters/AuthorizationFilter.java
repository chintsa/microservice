package com.suri.microservice.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by s.chintapalli on 7/1/2016.
 */
@AuthorizationNameBinding
@Provider
@Priority(Priorities.AUTHORIZATION)
public class AuthorizationFilter implements ContainerRequestFilter {

    private static Logger log = LoggerFactory.getLogger(AuthorizationFilter.class);

    @Context
    HttpServletRequest request;

    //@Context
    //HttpServletResponse response;

    @Context
    private ResourceInfo resourceInfo;

    @Context
    SecurityContext securityContext;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Method method = resourceInfo.getResourceMethod();
        String resourceName = method.getDeclaringClass().getName() + "." + method.getName();
        log.info("filter::Running authorization filter for resource " + resourceName);
        if(method.isAnnotationPresent(RolesAllowed.class)){
            if(!isUserAllowed(method))
                requestContext.abortWith(Response.status(Response.Status.FORBIDDEN)
                .entity("You cannot access the resource " + method.getName()).build());
        }
    }

    /**
     * Checks if user roles are matching roles mentioned in RolesAllowed annotation
     * @param method
     * @return
     */
    private boolean isUserAllowed(Method method){

        //TODO: Pass the request object to getDecryptAndValidateUser and get User POJO.
        //try {User userInfo = getDecryptAndValidateUser(request);}catch(Exception e){}
        //TODO: if userInfo is null then return false

        String resourceName = method.getDeclaringClass().getName() + "." + method.getName();
        RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
        Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));
        log.info("isUserAllowed::Roles allowed on " + resourceName + " are " + rolesSet.toString());
        rolesSet.stream().forEach(System.out::println);
        //TODO: check User roles again rolesSet and return true/false
        return true;
    }
}
