package com.suri.microservice;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Greg on 4/16/15.
 */
public class RestHeaderFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        httpServletResponse.addHeader("Pragma", "no-cache");
        httpServletResponse.addHeader("Cache-Control", "no-cache");
        httpServletResponse.addHeader("Expires", "0");
        //httpServletResponse.addHeader("Access-Control-Allow-Origin", Locator.getValue(properties, "Access-Control-Allow-Origin", "*"));  File has         httpServletResponse.addHeader("Access-Control-Allow-Origin", Access-Control-Allow-Origin=*);
        httpServletResponse.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE , OPTIONS, HEAD, PUT");
        httpServletResponse.addHeader("Access-Control-Allow-Headers", " * , accept, utoken, Content-Type, Content-Disposition, filename");
        httpServletResponse.addHeader("Access-Control-Allow-Credentials", "true");
        httpServletResponse.addHeader("X-XSS-protection", "1; mode=block");
        httpServletResponse.addHeader("X-Content-Type-Options", "nosniff");

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
