package com.suri.microservice;

import org.glassfish.jersey.server.ResourceConfig;


public class MyApplication extends ResourceConfig {

    public MyApplication() {
        // Add a package used to scan for components. //this is useful when you are using just the jersey framework
        packages(this.getClass().getPackage().getName() );
    }

}

