package com.suri.microservice.resource;

import com.suri.microservice.filters.AuthorizationNameBinding;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by s.chintapalli on 7/25/2016.
 */
@Path("/")
public class Root {

    public String name = "satya";

    /*private static void incrementHits(String url) {
        synchronized (hits) {
            AtomicInteger count = hits.get(url);
            if (count == null) {
                count = new AtomicInteger(0);
                hits.put(url, count);
            }
            int current = count.incrementAndGet();
            LOG.info("#Count " + current + " " + url);
        }
    }*/

    @AuthorizationNameBinding
    @GET
    @Produces("text/plain")
    @RolesAllowed({"Admin","Editor"})
    public Response getText() {

        /*
        Calling a resource hosted in a different server
        static HashMap<String, AtomicInteger> hits = new HashMap<>();
        store results in a hashmap
        static ConcurrentHashMap<String, Properties> propertiesCache = new ConcurrentHashMap<>();
        String url = SystemUtil.getProperty(Locator.URL_EXTERNAL) + file
        URL url = new URL(url)
        String key = url.toString();
        Properties properties = propertiesCache.get(key);
        if (properties != null) {
            return properties;
        }
        incrementHits(key);

        // Set timeout to 10 seconds since this call will hang if the server is being shutdown.
        URLConnection conn = url.openConnection();
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(10000);
        InputStream inputStream = conn.getInputStream();
        properties = new Properties();
        properties.load(inputStream);
        properties.setProperty(PROPERTY_FILE_NAME, url.getPath());
        propertiesCache.put(key, properties);
        return properties;


        In case of getting contents of a file
        static ConcurrentHashMap<String, String> fileContentsCache = new ConcurrentHashMap<>();
                String key = url.toString();
        String contents = fileContentsCache.get(key);
        if (contents != null) {
            return contents;
        }
        incrementHits(key);
        Set timeout to 10 seconds since this call will hang if the server is being shutdown.
        URLConnection conn = url.openConnection();
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(10000);
        InputStream inputStream = conn.getInputStream();
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer);
        contents = writer.toString();
        IOUtils.closeQuietly(inputStream);
        fileContentsCache.put(key, contents);
        return contents;
        */


        System.out.println(name);
        name = "new";
        return Response.ok("Hello world!").build();
    }


}
