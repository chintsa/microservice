package com.suri.microservice;

import com.suri.microservice.checks.MysqlHealthCheck;
import com.suri.microservice.resource.HelloWorldResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import javax.servlet.DispatcherType;
import javax.servlet.ServletRegistration;
import java.util.EnumSet;

/**
 * Created by s.chintapalli on 7/25/2016.
 */
public class DropwizardMain extends Application<DropwizardConfiguration> {

    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<DropwizardConfiguration> bootstrap) {
        super.initialize(bootstrap);
    }

    @Override
    public void run(DropwizardConfiguration configuration, Environment environment) throws Exception {
        HelloWorldResource helloWorldResource = new HelloWorldResource(configuration.getTemplate(), configuration.getDefaultName());
        environment.jersey().packages("com.suri.microservice.resource");
        environment.jersey().register(helloWorldResource);
        //http://localhost:9010/microservice/rest/hello-world
        //http://localhost:9010/microservice/rest/

        //if you are reading a windows path from yml then mention config: C:\Users\s.chintapalli\Downloads\sidekick_locator_config\sidekick_locator_config\
        //otherwise in linux config: /configs

        //environment.servlets().addServletListeners(new ShutdownContextListener());

        //environment.servlets().addFilter("RestHeaderFilter", new RestHeaderFilter()).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/rest/*");
        //jersey filter vs servlet filter

        ServletRegistration.Dynamic configServerServlet = environment.servlets().addServlet("SampleServlet",SampleServlet.class);
        configServerServlet.setLoadOnStartup(1);
        configServerServlet.addMapping("/*");
        //configServerServlet.setInitParameter("config",configuration.getConfig());
        ////http://localhost:9010/microservice/


        environment.healthChecks().register("dropwizard", new MysqlHealthCheck());

        System.setProperty("SYSTEMPROPERTY",configuration.getDefaultName());
        //read this one using System.getProperty(SYSTEMPROPERTY);
    }

    public static void main(String args[]) throws Exception{
        new DropwizardMain().run(args);
    }
}
