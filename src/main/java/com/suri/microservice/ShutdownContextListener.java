/**
 * Copyright (c) 2013 Attensity Corporation, All Rights Reserved
 */
package com.suri.microservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.lang.reflect.Method;

/**
 * Shutdown listener for Tomcat that releases resources (SCP-1843)
 *
 */

public class ShutdownContextListener implements ServletContextListener {
    private static Logger LOG = LoggerFactory.getLogger(ShutdownContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        LOG.debug("Bypass");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        String context = servletContextEvent.getServletContext().getContextPath();
        LOG.info("Shutting down context " + context);

        if (isClassLoaded("com.suri.db")) {
            //MySQLConnectionPool.destroyDataSource();
        }

        if (isClassLoaded("com.google.gson.Gson")) {
            //CommonUtil.unloadGson();
        }
    }

    /**
     * @param className
     * @return true/false
     */

    public static boolean isClassLoaded(String className) {
        try {
            Method m = ClassLoader.class.getDeclaredMethod("findLoadedClass", String.class);
            m.setAccessible(true);
            return m.invoke(ShutdownContextListener.class.getClassLoader(), className) != null;
        } catch (Throwable t) {
            LOG.warn("Unable to check if class is loaded: " + className);
            return false;
        }
    }

    /**
     * @param logger
     */

    public void setLogger(Logger logger) {
        ShutdownContextListener.LOG = logger;
    }
}
